#include "circuit.h"

void
Circuit::add_qubit(
    uint8_t id,
    uint8_t partition)
{
    if (this->qubits.size() < id + 1)
        this->qubits.resize(static_cast<unsigned long>(id + 1));
    if (this->qubits[id] == nullptr)
        this->qubits[id] = new Qubit(id, partition);
}

void
Circuit::add_cnot(
    uint8_t index,
    uint8_t control_index,
    uint8_t target_index)
{
    this->gates.emplace_back(new CnotGate(
        index,
        this->qubits[control_index],
        this->qubits[target_index]));
}

void
Circuit::add_hada(
    uint8_t index,
    uint8_t qubit_index)
{
    this->gates.emplace_back(new HadaGate(
        index,
        this->qubits[qubit_index]));
}

std::vector<Gate *>
Circuit::get_globals()
{
    std::vector<Gate *> res;

    for (const auto &gate : this->gates) {
        if (!gate->is_local())
            res.push_back(gate);
    }

    return res;
}

std::string
Circuit::to_string()
{
    uint8_t last_partition = 0;
    std::string res = "\n";

    auto add_empty_row = [&]()
    {
        auto size = this->gates.size();
        res += "  ";

        while (size--)
            res += "¦   ¦";
        res += "\n";
    };

    for (uint8_t i = 0; i < this->qubits.size(); ++i) {
        if (this->qubits[i]->partition != last_partition) {
            last_partition = this->qubits[i]->partition;
            add_empty_row();
            add_empty_row();
        }
        add_empty_row();
        res += std::to_string(i) + " ";
        for (auto const &g : this->gates) {
            if (auto *gate = dynamic_cast<CnotGate *>(g)) {
                if (gate->control->id == i) {
                    res += "¦―•―¦";
                    continue;
                }
                if (gate->target->id == i) {
                    res += "¦―⊕―¦";
                    continue;
                }
            }
            if (auto *gate = dynamic_cast<HadaGate *>(g)) {
                if (gate->qubit->id == i) {
                    res += "¦―□―¦";
                    continue;
                }
            }
            res += "¦―――¦";
        }
        res += "\n";
    }
    add_empty_row();

    return res + "\n";
}
