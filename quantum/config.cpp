#include "config.h"
#include "cnot_gate.h"
#include "hada_gate.h"

Config::Config(std::vector<Gate *> gates)
{
    this->size = static_cast<uint8_t>(gates.size());
    this->config = new uint8_t[this->size];
    this->base = 0;

    for (int i = 0; i < this->size; ++i) {
        if (auto gate = dynamic_cast<CnotGate *>(gates[i])) {
            if (gate->control->partition > this->base - 1)
                this->base = static_cast<uint8_t>(gate->control->partition + 1);
            if (gate->target->partition > this->base - 1)
                this->base = static_cast<uint8_t>(gate->target->partition + 1);
        }
        if (auto gate = dynamic_cast<HadaGate *>(gates[i])) {
            if (gate->qubit->partition > this->base - 1)
                this->base = static_cast<uint8_t>(gate->qubit->partition + 1);
        }
        if (gates[i]->is_local())
            this->config[i] = 255;
        else
            this->config[i] = 0;
    }
}

uint8_t Config::operator[](uint8_t index)
{
    return this->config[index];
}

std::string Config::to_string()
{
    std::string temp;
    for (int i = 0; i < this->size; ++i) {
        if(this->config[i] != 255)
            temp += std::to_string(this->config[i]);
    }
    return temp;
}

bool Config::next()
{
    auto inc = [this](uint8_t digit)
    {
        if (digit < (this->base - 1))
            return ++digit;
        return static_cast<uint8_t>(0);
    };

    uint8_t i = this->size;
    while (--i) {
        if (this->config[i] != 255) {
            this->config[i] = inc(this->config[i]);
            if (this->config[i])
                return true;
        }
    }
    return false;
}

Config::Config(uint8_t config[], uint8_t size, uint8_t base)
{
    this->config = config;
    this->size = size;
    this->base = base;
}
