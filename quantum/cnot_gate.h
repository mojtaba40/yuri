#ifndef QUANTUM_CNOTGATE_H
#define QUANTUM_CNOTGATE_H


#include "gate.h"
#include "qubit.h"

class CnotGate: public Gate
{
public:
    Qubit *control;
    Qubit *target;

    CnotGate();
    CnotGate(uint8_t index, Qubit *control, Qubit *target);
    ~CnotGate();

    bool is_local() override;
    std::string to_string() override;
}; //class CnotGate

#endif //QUANTUM_CNOTGATE_H
