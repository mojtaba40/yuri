#ifndef QUANTUM_HADA_GATE_H
#define QUANTUM_HADA_GATE_H


#include "gate.h"
#include "qubit.h"

class HadaGate: public Gate
{
public:
    Qubit *qubit;

    HadaGate();
    HadaGate(uint8_t index, Qubit *qubit);
    ~HadaGate();

    bool is_local() override;
    std::string to_string() override;
}; //class HadaGate


#endif //QUANTUM_HADA_GATE_H
