#ifndef QUANTUM_CIRCUIT_H
#define QUANTUM_CIRCUIT_H


#include <sstream>
#include <vector>
#include <string>

#include "cnot_gate.h"
#include "hada_gate.h"
#include "qubit.h"

class Circuit
{
public:
    std::vector<Qubit *> qubits;
    std::vector<Gate *> gates;

    void add_qubit(uint8_t id, uint8_t partition);
    void add_cnot(uint8_t index, uint8_t control_index, uint8_t target_index);
    void add_hada(uint8_t index, uint8_t qubit_index);
    std::vector<Gate *> get_globals();
    std::string to_string();
}; //class circuit


#endif //QUANTUM_CIRCUIT_H
