#include "hada_gate.h"

HadaGate::HadaGate()
{
    this->index = 255;
    this->qubit = nullptr;
}

HadaGate::HadaGate(
    uint8_t index,
    Qubit *qubit)
{
    this->index = index;
    this->qubit = qubit;
}

HadaGate::~HadaGate()
{
    delete this->qubit;
}

bool
HadaGate::is_local()
{
    return true;
}

std::string
HadaGate::to_string()
{
    return "HADA"
        + std::to_string(this->index)
        + "("
        + std::to_string(this->qubit->partition)
        + ", "
        + std::to_string(this->qubit->id)
        + ")";
}
