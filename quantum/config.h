#ifndef QUANTUM_CONFIG_H
#define QUANTUM_CONFIG_H


#include <cstdint>
#include <vector>

#include "gate.h"

class Config
{
public:
    explicit Config(std::vector<Gate *> gates);
    Config(uint8_t* config, uint8_t size, uint8_t base);
    ~Config() = default;

    uint8_t operator[](uint8_t index);
    std::string to_string();
    bool next();

private:
    uint8_t size;
    uint8_t base;
    uint8_t *config;
};


#endif //QUANTUM_CONFIG_H
