#include "cnot_gate.h"


CnotGate::CnotGate()
{
    this->index = 255;
    this->control = nullptr;
    this->target = nullptr;
}

CnotGate::CnotGate(
    uint8_t index,
    Qubit *control,
    Qubit *target)
{
    this->index = index;
    this->control = control;
    this->target = target;
}

CnotGate::~CnotGate()
{
    delete this->control;
    delete this->target;
}

bool
CnotGate::is_local()
{
    return this->control->partition == this->target->partition;
}

std::string
CnotGate::to_string()
{
    return "CNOT"
        + std::to_string(this->index)
        + "("
        + std::to_string(this->control->partition)
        + ", "
        + std::to_string(this->control->id)
        + ", "
        + std::to_string(this->target->partition)
        + ", "
        + std::to_string(this->target->id)
        + ")";
}
