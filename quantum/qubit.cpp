#include "qubit.h"

Qubit::Qubit(
    uint8_t id,
    uint8_t partition)
{
    this->partition = partition;
    this->id = id;
}

Qubit::Qubit()
{
    this->partition = 255;
    this->id = 255;
}
