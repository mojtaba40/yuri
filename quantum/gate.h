#ifndef QUANTUM_GATE_H
#define QUANTUM_GATE_H


#include <string>

class Gate
{
public:
    uint8_t index = 0;

    virtual bool is_local() = 0;
    virtual std::string to_string() = 0;
}; //class Gate


#endif //QUANTUM_GATE_H
