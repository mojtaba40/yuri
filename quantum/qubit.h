#ifndef QUANTUM_QUBIT_H
#define QUANTUM_QUBIT_H


#include <vector>
#include <cstdint>

#include "gate.h"

class Qubit
{
public:
    uint8_t partition;
    uint8_t id;

    Qubit(uint8_t id, uint8_t partition);
    Qubit();
}; //class Qubit


#endif //QUANTUM_QUBIT_H
