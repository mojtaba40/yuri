#ifndef QUANTUM_CSV_H
#define QUANTUM_CSV_H

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

namespace csv
{

class Parser
{
private:
    std::ifstream file;

    bool valid_char(char);
    std::string clean_value(std::string value);

public:
    explicit Parser(const std::string &filename);
    bool get_row(std::vector<std::string> *);

}; //class Parser

} //namespace csv

#endif //QUANTUM_CSV_H
