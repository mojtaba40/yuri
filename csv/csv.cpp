#include "csv.h"

csv::Parser::Parser(const std::string &filename)
{
    this->file = std::ifstream(filename);
}

bool csv::Parser::valid_char(char c)
{
    char valid[] = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (auto v : valid)
        if (c == v)
            return true;
    return false;
}

std::string csv::Parser::clean_value(std::string value)
{
    for (auto c: value)
        if (!valid_char(c))
            value.erase(
                std::remove(value.begin(), value.end(), c),
                value.end());
    return value;
}

bool csv::Parser::get_row(std::vector<std::string> *row)
{
    std::string line, field;
    row->clear();

    if (!std::getline(this->file, line))
        return false;

    std::istringstream line_stream(line);
    while (getline(line_stream, field, ','))
        row->push_back(clean_value(field));

    return true;
}
