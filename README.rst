Yuri
====
Simulating a Distributed Quantum Circuit and implement an algorithm to optimize teleportation cost base on `"Optimizing Teleportation Cost in Distributed Quantum Circuits" <https://arxiv.org/abs/1605.07935>`_ by Mariam Zomorodi-Moghadam, Monireh Houshmand, and Mahboobeh Houshmand

Build
-----
CMake’s documentation strongly suggests that out-of-source builds be done rather than in-source builds. I agree as it makes it much easier to convince yourself that your build has really been cleaned since you can simply delete the build folder and start over. But here I go with in-source build to make it more simple.

.. code-block:: shell

    cmake -G "Unix Makefiles"
    make

Run
---
Program needs a circuit as input. You can find an example in folder named "sample". Simply copy that wherever your execute is and after that run the program.

.. code-block:: shell

    cp sample/circuit.csv ./
    ./yuri
