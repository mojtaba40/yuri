#include <iostream>

#include "csv/csv.h"
#include "quantum/circuit.h"
#include "quantum/config.h"

auto read_circuit(
    const std::string &filename)
{
    csv::Parser parser(filename);
    std::vector<std::string> row;
    Circuit circuit = Circuit();
    uint8_t index = 0;

    auto to_uint8 = [](const std::string &str)
    {
        uint16_t num_16;
        std::stringstream(str) >> num_16;
        return (uint8_t) num_16;
    };

    while (parser.get_row(&row)) {
        if (row[0] == "cnot") {
            circuit.add_qubit(to_uint8(row[1]), to_uint8(row[2]));
            circuit.add_qubit(to_uint8(row[3]), to_uint8(row[4]));
            circuit.add_cnot(index++, to_uint8(row[1]), to_uint8(row[3]));
        }
        else if (row[0] == "hada") {
            // TODO: read qubit as complex number
            circuit.add_qubit(to_uint8(row[1]), to_uint8(row[2]));
            circuit.add_hada(index++, to_uint8(row[1]));
        }
        else if (row[0] == "Type") {
            continue;
        }
        else {
            throw std::invalid_argument(
                '"' + row[0] + '"' + "is not supported"
            );
        }
    }

    return circuit;
}

auto non_execute(
    Gate *gate1,
    Gate *gate2,
    Config &config)
{
    auto *c1 = dynamic_cast<CnotGate *>(gate1);
    auto *c2 = dynamic_cast<CnotGate *>(gate2);
    auto *h1 = dynamic_cast<HadaGate *>(gate1);
    auto *h2 = dynamic_cast<HadaGate *>(gate2);

    if (h1 || h2)
        return false;

    if (!c2->is_local()) {
        // If labels are same
        if (config[c2->index] == config[c1->index]) {
            // Check if it need another teleportation

            // If target is migrated
            if (config[c2->index] == c2->control->partition) {
                // If target is migrated
                if (config[c1->index] == c1->control->partition) {
                    if (c2->target->id != c1->target->id) {
                        return true;
                    }
                }
                else { // Control is migrated
                    if (c2->target->id != c1->control->id) {
                        return true;
                    }
                }
            }
            else { // Control is migrated
                // If target is migrated
                if (config[c1->index] == c1->control->partition) {
                    if (c2->control->id != c1->target->id) {
                        return true;
                    }
                }
                else { // Control is migrated
                    if (c2->control->id != c1->control->id) {
                        return true;
                    }
                }
            }
        }
        else { // Labels are different
            return true;
        }
    }
    else { // If it's local
        // Check if one of qubits is the same as the migrated qubit
        if (config[c1->index] == c1->control->partition) { // Target is migrated
            if (c2->target->id == c1->target->id)
                return true;
        }
        else { // Control is migrated
            if (c2->control->id == c1->control->id)
                return true;
        }
    }

    return false;
};

auto non_commute(
    Gate *gate1,
    Gate *gate2)
{
    auto *c1 = dynamic_cast<CnotGate *>(gate1);
    auto *c2 = dynamic_cast<CnotGate *>(gate2);
    auto *h1 = dynamic_cast<HadaGate *>(gate1);
    auto *h2 = dynamic_cast<HadaGate *>(gate2);

    if (c1 && c2) {
        if (c1->target == c2->control || c1->control == c2->target)
            return true;
    }
    if (c1 && h2) {
        if (c1->control == h2->qubit)
            return true;
        if (c1->target == h2->qubit)
            return true; // FIXME: there should be another condition
    }
    if (h1 && c2) {
        if (c2->control == h1->qubit)
            return true;
        if (c2->target == h1->qubit)
            return true; // FIXME: there should be another condition
    }
    return false;
};

auto print_teleportation(Gate *gate, Config &config)
{
    auto *cnot = dynamic_cast<CnotGate *>(gate);
    std::cout << "g" << (uint16_t) cnot->index + 1 << "(";
    if (config[cnot->index] == cnot->control->partition) { // Target is migrated
        std::cout << "T";
    }
    else { // Control is migrated
        std::cout << "C";
    }
    std::cout << ") ";
}

auto min_teleportation(
    Circuit circuit, // TODO: change it to pointer
    Config &config,
    uint8_t total)
{
    bool sw = false;

    if (circuit.gates.empty())
        return total;

    if (circuit.gates[0]->is_local()) {
        circuit.gates.erase(circuit.gates.begin());
        return min_teleportation(circuit, config, total);
    }

    // put first gate to temp then print and remove it
    Gate *temp = circuit.gates[0];
    print_teleportation(circuit.gates[0], config);
    circuit.gates.erase(circuit.gates.begin());

    for (uint8_t i = 0; i < circuit.gates.size(); ++i) {
        if (!non_execute(temp, circuit.gates[i], config)) {
            for (uint8_t k = 0; k < i; ++k) {
                if (non_commute(circuit.gates[i], circuit.gates[k])) {
                    sw = true;
                    break;
                }
            }
            if (!sw)
                circuit.gates.erase(circuit.gates.begin() + i--);
        }
    }
    total = static_cast<uint8_t>(total + 2);
    return min_teleportation(circuit, config, total);
}

auto main()
{
    Circuit circuit;
    circuit = read_circuit("circuit.csv");
    Config config(circuit.gates);
//    uint8_t c[] = {255, 0, 1, 255, 0, 255, 0, 255, 1};
//    Config config(c, 9, 2);

    std::cout << circuit.to_string();

    do {
        std::cout << config.to_string();
        std::cout << " | ";

        uint8_t min = min_teleportation(circuit, config, 0);
        std::cout << "= " << (uint16_t) min << "\n";
    }
    while (config.next());

    std::cout << "\nDone!";
}
